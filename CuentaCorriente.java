
/**
 * Práctica 2 - PCTR 2020
 * 
 * @author Iván Magariño Aguilar
 * @version 1.0
 */

public class CuentaCorriente {

    protected int id;
    protected String titular;
    private double saldo;

    public CuentaCorriente () {} //Constructor vacío

    /**
     * Constructor de la clase CuentaCorriente.
     * @param id Entero
     * @param titular String
     * @param saldo Double
     */
    public CuentaCorriente (int id,
                            String titular,
                            double saldo) {

        this.id = id;
        this.titular = titular;
        this.saldo = saldo;
    }

    public int getId() {return (id); }

    public double getSaldo() { return (saldo); }

    public void deposito (double cantidad) {
        saldo += cantidad;
    }

    public boolean reintegro (double cantidad) {
        if (cantidad>saldo)
            return (false);
        else {
            saldo-=cantidad;
            return (true);
        }
    }
}
