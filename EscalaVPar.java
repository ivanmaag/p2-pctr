
/**
 * Práctica 2 - PCTR
 * 2020
 * 
 * @author Iván Magariño Aguilar
 * @version 1.0
 */

public class EscalaVPar implements Runnable {

    private int tipoHilo;
    private static int [] v = new int[100000000];
    
    /**
     * Constructor de la clase EscalaVPar.
     * @param tipoHilo Entero
     */
    public EscalaVPar(int tipoHilo) {
        this.tipoHilo = tipoHilo;
    }

    //Método run
    public void run() {
        //Multiplico cada valor del vector por 2
        switch (tipoHilo) {
        case 0: for (int i=0; i<v.length/2; i++) { //Trabajamos desde la posicion 0 hasta la mitad del vector.
                    v[i] *= 2;
                }
                break;
        case 1: for (int i=v.length/2; i<v.length; i++) { //Trabajamos a partir de la mitad del vector hasta el final.
                    v[i] *= 2;
                }
                break;
        }
    }

    /**
     * Método main. Rellena un vector y lo escala de forma paralela. Crea 2 objetos de la clase EscalaVPar y crea 2 hilos de la clase Thread y los lanza.
     * @param args No recoge argumentos.
     * @throws InterruptedException Necesaria para utilizar el método join.
     */
    public static void main(String[] args) throws InterruptedException {
        
        //Inicializamos el vector todo a 3
        for (int i=0; i<v.length; ++i) {
            v[i] = 3;
        }

        EscalaVPar t1 = new EscalaVPar(0);
        EscalaVPar t2 = new EscalaVPar(1);
        Thread h1 = new Thread(t1);
        Thread h2 = new Thread(t2);
        h1.start();
        h2.start();
        h1.join();
        h2.join();

        System.out.print("Vector escalado");
    }
}
