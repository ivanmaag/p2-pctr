
/**
 * Práctica 2 - PCTR
 * 2020
 * 
 * @author Iván Magariño Aguilar
 * @version 1.0
 */

public class UsaHebra {

    public static final int N = 10000000;

    /**
     * Método main. Crea 2 objetos de la clase Hebra, los lanza e imprime por pantalla el resultado obtenido.
     * @param args No recoge argumentos.
     * @throws InterruptedException Necesaria para utilizar el método join.
     */
    public static void main(String[] args) throws InterruptedException {
        
        Hebra hebra1 = new Hebra(N, 0);
        Hebra hebra2 = new Hebra(N, 1);
        hebra1.start();
        hebra2.start();
        hebra1.join();
        hebra2.join();

        System.out.println(hebra1.getN());
    }
}
