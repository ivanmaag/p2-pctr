/**
 * Práctica 2 - PCTR
 * 2020
 * 
 * @author Iván Magariño Aguilar
 * @version 1.0
 */

public class TareaRunnable implements Runnable {

    protected static int n=0; //Variable de clase

    //Constructor vacío
    public TareaRunnable() {}

    //Método run
    public void run() {
        for (int i=0; i<10000000; i++) {
            inc(); dec();
        }
    }

    //Observadores
    public int vDato() { return n; }
    
    //Modificadores
    public void inc() { n++; }
    public void dec() { n--; }
}
