/**
 * Práctica 2 - PCTR
 * 2020
 * 
 * @author Iván Magariño Aguilar
 * @version 1.0
 */

public class EscalaVector {

    private static int [] v = new int[100000000];
    
    /**
     * Método main. Rellena un vector y lo escala de forma secuencial.
     * @param args No recoge argumentos.
     */
    public static void main(String[] args) {
        
        //Inicializamos el vector todo a 3
        for (int i=0; i<v.length; ++i) {
            v[i] = 3;
        }

        //Multiplicamos cada valor del vector por 2
        for (int i=0; i<v.length; ++i) {
            v[i] *= 2;
        }

        System.out.print("Vector escalado");
    }
}
