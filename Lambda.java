
/**
 * Práctica 2 - PCTR 2020
 * 
 * @author Iván Magariño Aguilar
 * @version 1.0
 */

public class Lambda {

    private static int n=0; //Variable de clase

    //Modificadores
    public static void inc() { n++; }
    public static void dec() { n--; }
    
    //Observadores
    public static int getN() {
        return n;
    }

    /**
     * Método main. Se utilizan expresiones lambda para incrementar y decrementar la variable compartida n.
     * @param args No recoge argumentos.
     */
    public static void main(String[] args) throws InterruptedException {

        //Expresión lambda de una primera forma 
        Runnable t1 = new Runnable() {
            @Override
            public void run() {
                for (int i = 0; i < 10; i++) {
                    inc(); dec();
                }
            }
        };

        //Expresión lambda de otra forma
        Runnable t2 = () -> {
            for (int i = 0; i < 10; i++) {
                inc(); dec();
            }
        };
        
        //Se crean los hilos y se lanzan
        Thread h1 = new Thread(t1);
        Thread h2 = new Thread(t2);
        h1.start();
        h2.start();
        h1.join();
        h2.join();

        System.out.println(getN());
    }
}
