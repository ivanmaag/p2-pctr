
/**
 * Práctica 2 - PCTR 2020
 * 
 * @author Iván Magariño Aguilar
 * @version 1.0
 */

public class Cajero implements Runnable {

	private int tipo;
	private double cantidad;
	private CuentaCorriente cuenta;

	/**
     * Constructor de la clase Cajero.
     * @param CuentaCorriente Objeto CuentaCorriente
     * @param tipo Entero
     * @param Cantidad Double
     * @return Referencia a un objeto de tipo Cajero
     * @exception No No lanza excepciones
	 */
    public Cajero(CuentaCorriente cuenta,
                  int tipo,
                  double cantidad) {

		this.cuenta = cuenta;
		this.tipo = tipo;
		this.cantidad = cantidad;
	}

	public void run() {
		switch (tipo) {
			case 1: cuenta.reintegro(cantidad);
				break;
    		case 2: cuenta.deposito(cantidad);
				break;
		}
	}
}
