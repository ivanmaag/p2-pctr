
/**
 * Práctica 2 - PCTR
 * 2020
 * 
 * @author Iván Magariño Aguilar
 * @version 1.0
 */

public class Hebra extends Thread {
    
    protected static int n=0; //Variable de clase
    private int nIter;
    protected int tipoHebra;

    /**
     * Constructor de la clase Hebra.
     * @param nIter Entero
     * @param tipoHebra Entero
     */
    public Hebra(int nIter,
                 int tipoHebra) {

        this.nIter = nIter;
        this.tipoHebra = tipoHebra;
    }

    //Método run
    public void run() {
        switch(tipoHebra) {
            case 0: for (int i=0; i<=nIter; ++i)
                        n++;
                    break;
            case 1: for (int i=0; i<=nIter; ++i)
                        n--;
                    break;
        }
    }

    public int getN() { return n; }
}
