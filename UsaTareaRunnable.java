/**
 * Práctica 2 - PCTR
 * 2020
 * 
 * @author Iván Magariño Aguilar
 * @version 1.0
 */

 public class UsaTareaRunnable {

    /**
     * Método main. Crea 2 objetos de la clase TareaRunnable y crea 2 hilos de la clase Thread, los lanza e imprime por pantalla el resultado obtenido.
     * @param args No recoge argumentos.
     * @throws InterruptedException Necesaria para utilizar el método join.
     */
    public static void main(String[] args) throws InterruptedException {

        TareaRunnable tarea1 = new TareaRunnable();
        TareaRunnable tarea2 = new TareaRunnable();
        Thread h1 = new Thread(tarea1);
        Thread h2 = new Thread(tarea2);
        h1.start();
        h2.start();
        h1.join();
        h2.join();

        System.out.println(tarea1.vDato());
    }
}
