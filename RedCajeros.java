
/**
 * Práctica 2 - PCTR 2020
 * 
 * @author Iván Magariño Aguilar
 * @version 1.0
 */

public class RedCajeros {

	/** Método main donde se encuentra el código correspondiente al hilo principal del programa.
	 * @param args Array de cadenas que almacena los comandos de consola.
	 * @exception InterruptedException Excepción lanzada por el método join si algún hilo hijo por el que se estaba esperando ha sido interrumpido.
	 */
	public static void main(String[] args) throws InterruptedException {

		CuentaCorriente cuentaIvan = new CuentaCorriente(1, "Ivan", 10000);
		Cajero cajIngr = new Cajero(cuentaIvan, 0, 20);
		Cajero cajSaca = new Cajero(cuentaIvan, 1, 20);
		Thread cajeros[] = new Thread[1000];
		
		for (int i=0; i < cajeros.length; i++)
            if (i < cajeros.length / 2)
                cajeros[i] = new Thread(cajIngr);
            else
                cajeros[i] = new Thread(cajSaca);
		
		System.out.println("Saldo antes: " + cuentaIvan.getSaldo());
		
		for(int i=0; i < cajeros.length; i++) cajeros[i].start();
		for(int i=0; i < cajeros.length; i++) cajeros[i].join();
		
        System.out.println("Saldo después: " + cuentaIvan.getSaldo());
    }
}
